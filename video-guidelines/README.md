# Video Guidelines

## How to upload existing project to newly created repository 

### 1. GIT Project Upload Demo Method 1

This video contains the instructions about how to upload your existing project on the newly created GIT repository. 
In this video you will see that I have used the option `--allow-unrelated-histories`, I strongly suggest not using this option
when you already have many commits in your repository. You should use this option only for the very first time you are uploading your project.

[GIT Project Upload Demo Method 1](https://drive.google.com/file/d/1CgZt6W90DlLJxqX6fqUZf16yYLQPbES-/view)



### 2. GIT Project Upload Demo Method 2

This video contains the instructions about how to upload your existing project on the newly created GIT repository.
You should follow this method only when you are uploading your project in a new repository and when there are no existing commits of project
source by someone else.

[GIT Project Upload Demo Method 2](https://drive.google.com/file/d/1-bi93bL0pjENtBcy6lrkeLcr9K7_8mVD/view)



