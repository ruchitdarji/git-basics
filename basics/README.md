# git-basics

## Basics

### To clone repository over local machine

```bash
git clone <repo-url>
```

### To create a new branch

```bash
git checkout -b <branch-name>
```

### To add(i.e. stash) and commit files

```bash
git add -A
git commit -m "meaningful-message"
```

### To pull changes from remote server

```bash
git pull <remote-upstream-branch> <local-branch>
# e.g. git pull origin master
```

### To push changes over remote server

```bash
git push <remote-upstream-branch> <local-branch>
# e.g. git push origin master
```

### Configuration

To list out current configuration of repository setup over working directory.
```bash
git config -l
```

To set configuration specific to local directory
```bash
git config user.name "Ruchit Darji"
git config user.email "ruchit.darji@iflair.com"
```

To set configuration globally to your system
```bash
git config --global user.name "Ruchit Darji"
git config --global user.email "ruchit.darji@iflair.com"
```

### Branch specific

To get list of branches within local
```bash
git show-branch
```

To view remote branches
```bash
git branch -r
```

To change remote URL
```bash
git remote set-url origin <new-remote-url>
```

### To check history of commits

```bash
git log
```

### To check current status of repository

```bash
git status
```

### To remove(i.e. unstash) files from git which is not yet pushed over repository

```bash
git reset <file-path>
```

### To reset file changes to previous version

```bash
git checkout <file-path>
```

### To modify last commit message [if added wrong message]

```bash
git commit --amend -m "New message"
```

## Advanced

### Conflicts

In case of conflicts, check the conflict files and removed unnecessary text writted and modify script according to app requirement and then commit changes and push over repository.

### Ignore files (i.e. gitignore)

We may need to ignore several files/directories such as cache, backup files, etc. then create a `.gitignore` file on root directory of project. (You can also put that file in any directory accordingly)

[gitignore templates](https://github.com/github/gitignore)

Let us say, you've added several directories by mistake over repository which is no longer should be present on repository, then remove:
```bash
git rm --cached <file-name/or/directory>
```

### To rename file

In case if you directly rename file within your OS then it will be treated differently (old file deleted/new file created). To avoid such thing, use below:
```bash
git mv <old-file-name> <new-file-name>
```

### To ignore filemode changes

If you change file permission then also it will be treated differently. To ignore such thing (though it is not recommended)
```bash
git config core.fileMode false
```

### To view list of commits specific to a file

```bash
git blame <file-path>
```

### To see which file I have modified

```bash
git whatchanged --author=Ruchit Darji
```

### To discard/delete last commit from remote repository & local repository and remove changes from files too

```bash
git reset --hard HEAD~1
git push origin master -f
```

### To discard/delete last commit but retain changes in files

```bash
git reset --soft HEAD~1
```

### To get someone's change from another branch to your current branch:

```bash
git checkout "branch-name-to-take-file-change" "file-path"
```

This technique is helpful when we want to test someone's change within your current branch without switching branches.

### To remove file from git commit after push:

```bash
git checkout HEAD^ -- /path/to/file
git commit --amend
git push -f
```
